process.env.UV_THREADPOOL_SIZE = 128;

var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');

var jsonArray = [];
var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page 
    callback : function (error, res, done) {
        if(error){
            console.log("ERROR:");
            console.log(error);
        }else{
            var $ = res.$;
            // $ is Cheerio by default 
            // a lean implementation of core jQuery designed specifically for the server 
          
            var jsonOutput = {};
            
            // console.log(res);

            jsonOutput.label = $("#pd-product-details-component").find(".pd-Attribute h3.pd-Attribute__label").text();
            jsonOutput.zutaten = $("#pd-product-details-component").find(".pd-ProductDetails.pd-content--primary.rs-qa-primary-content.pd-product-detail .pd-Textcordion div .pd-AttributeGroup .pd-AttributeGroup div.pd-Attribute:first-child").text();
            jsonOutput.allergene = $("#pd-product-details-component").find(".pd-ProductDetails.pd-content--primary.rs-qa-primary-content.pd-product-detail .pd-Textcordion div .pd-AttributeGroup .pd-AttributeGroup div.pd-Attribute:last-child").text();
            // Dopplungen verhindern
            if(jsonOutput.zutaten == jsonOutput.allergene){
              jsonOutput.allergene = '';
            }

            // jsonOutput.zutaten = $('#pd-product-details-component').children().children('.pd-Textcordion').first().children('pd-AttributeGroup').children('pd-AttributeGroup').first().text();
            // jsonOutput.href = $(this).children('.rs-media__body').children('h2').children('a').attr('href');
   
            jsonArray.push(jsonOutput);

            console.log(jsonOutput);

        
            // fs.appendFile('reweDetailData.json', JSON.stringify(jsonArray), function (err) {
            //   if (err) throw err;
            //     console.log('The "data to append" was appended to file!');
            // });
        
        }
        done();
    }
});


 
// Queue just one URL, with default callback 
c.queue('https://shop.rewe.de/ja-schokodessert-mit-sahne-200g/PD1063147');