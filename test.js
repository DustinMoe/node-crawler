process.env.UV_THREADPOOL_SIZE = 128;

var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');

var jsonArray = [];
var rangeNumber = 1;
var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page 
    callback : function (error, res, done) {
        if(error){
            console.log("ERROR:");
            console.log(error);
        }else{
            var $ = res.$;
            // $ is Cheerio by default 
            // a lean implementation of core jQuery designed specifically for the server 
            $('.rs-tile__content').each(function(index, a) {
              var jsonOutput = {};
     
              jsonOutput.name = $(this).children('.rs-media__body').children('h2').children('a').text();
              jsonOutput.href = $(this).children('.rs-media__body').children('h2').children('a').attr('href');
     
              jsonArray.push(jsonOutput);
     
            });

            rangeNumber++;
            console.log(rangeNumber);
        
            var toQueueUrl = 'https://shop.rewe.de/productList?search=&sorting=RELEVANCE&startPage='+ rangeNumber +'&selectedFacets=';
            // Statt der 10 sollte hier dynamisch die höchste Seite besorgt werden
            if (parseInt(rangeNumber) < 10) {
              c.queue(toQueueUrl);  
            } else {
              fs.appendFile('reweData.json', JSON.stringify(jsonArray), function (err) {
                if (err) throw err;
                console.log('The "data to append" was appended to file!');
              });
            }

        }
        done();
    }
});


 
// Queue just one URL, with default callback 
c.queue('https://shop.rewe.de/productList?search=&sorting=RELEVANCE&startPage=1&selectedFacets=');