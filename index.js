var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');
 
var jsonArray = [];
 
var craigslist = new Crawler({
    maxConnections : 10,
    callback : function (error, result, $) {
 
        $('.row').each(function(index, a) {
          var jsonOutput = {};
 
          var dataSplit = $(a).text().split(' ');
          jsonOutput.price = dataSplit[1];
          jsonOutput.date = dataSplit[5] + dataSplit[6];
          jsonOutput.siteLink = "http://sfbay.craigslist.org" + $(a).children().attr('href');
 
          console.log("price:" + jsonOutput.price + "  date:" + jsonOutput.date 
                      + "   date:" + jsonOutput.siteLink)
 
          jsonArray.push(jsonOutput)
 
        });
        
        var rangeNumber = $($('.range')[0]).text().split(' ')[2]
        
        console.log("We are on item range: " + rangeNumber)
        
        var toQueueUrl = 'http://sfbay.craigslist.org/search/bia?s=' + rangeNumber
 
        if (parseInt(rangeNumber) < 1000) {
          craigslist.queue(toQueueUrl);  
        } else {
          fs.appendFile('craigsListData.txt', JSON.stringify(jsonArray), function (err) {
            if (err) throw err;
            console.log('The "data to append" was appended to file!');
          });
        }
    }
});
 
craigslist.queue('http://sfbay.craigslist.org/search/bia')
